-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 26, 2019 at 07:57 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rekap`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_bni`
--

CREATE TABLE IF NOT EXISTS `file_bni` (
`fbniId` int(11) NOT NULL,
  `fbniFlimId` int(11) NOT NULL,
  `fbniAccountNo` varchar(100) NOT NULL,
  `fbniPostDate` datetime NOT NULL,
  `fbniValueDate` datetime NOT NULL,
  `fbniBranch` varchar(100) NOT NULL,
  `fbniJournalNo` varchar(100) NOT NULL,
  `fbniDescription` text NOT NULL,
  `fbniDebit` varchar(250) NOT NULL,
  `fbniCredit` varchar(250) NOT NULL,
  `fbniVA` varchar(100) NOT NULL,
  `fbniVACustomer` varchar(200) NOT NULL,
  `fbniCreateDate` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `file_import`
--

CREATE TABLE IF NOT EXISTS `file_import` (
`flimId` int(11) NOT NULL,
  `flimStatus` int(11) NOT NULL DEFAULT '0',
  `flimTotalData` int(11) DEFAULT NULL,
  `flimSource` varchar(100) NOT NULL,
  `flimName` varchar(255) NOT NULL,
  `flimDate` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_bni`
--
ALTER TABLE `file_bni`
 ADD PRIMARY KEY (`fbniId`), ADD KEY ` fbniFlimId` (`fbniFlimId`), ADD KEY `fbniJournalNo` (`fbniJournalNo`);

--
-- Indexes for table `file_import`
--
ALTER TABLE `file_import`
 ADD PRIMARY KEY (`flimId`), ADD UNIQUE KEY `flimName` (`flimName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_bni`
--
ALTER TABLE `file_bni`
MODIFY `fbniId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `file_import`
--
ALTER TABLE `file_import`
MODIFY `flimId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
