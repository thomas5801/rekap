<?php
    
    require __DIR__ . '/config.php';

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if(!isset($_GET['date'])){
       header('Location: upload.php');
       exit;
    }
    
    
    $date = $_GET['date'];
    $date = urlencode($_GET['date']);



    $ch = curl_init(); 
    
    $url_ = $url."api/v1/laporan_bni_va/".$date ; 


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl

    $data_all = json_decode($output);
    
    foreach($data_all->data as $vall){
        

        $v0 = $vall->bnivId;
        $v1 = $vall->bnivOrderId;
        $v2 = $vall->bnivNtb;
        $v3 = $vall->bnivVANumber;
        $v4 = $vall->bnivEvent;
        $v5 = $vall->bnivEventId;
        $v6 = $vall->bnivUpdatedTime;
        
        $sql = "INSERT INTO laporan_bni_virtual_accounts (bnivId, bnivOrderId, bnivNtb, bnivVANumber, bnivEvent, bnivEventId, bnivUpdatedTime) VALUES ('$v0', '$v1', '$v2', '$v3', '$v4', '$v5', '$v6');";
        $conn->query($sql);
    
    }
    
    mysqli_close($conn);
    
    header('Location: upload.php');
    exit;
?>